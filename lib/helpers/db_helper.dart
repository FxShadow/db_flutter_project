import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DbHelper {
  static final DbHelper instance = DbHelper._instance();

  DbHelper._instance();

  static Database? _db;

  Future<Database> get db async {
    _db ??= await initDb();
    return _db!;
  }

  static Future<Database> initDb() async {
    final db = openDatabase(
      join(await getDatabasesPath(), 'activity_2.db'),
      onCreate: (db, version) async {
        await db.execute(
          'CREATE TABLE products(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, price INTEGER, quantity INTEGER, stated_at INTEGER)', // CREATE TABLE clients(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, surname TEXT, email TEXT, phone INTEGER, address TEXT, stated_at INTEGER),
        );
        await db.execute(
          'CREATE TABLE clients(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, surname TEXT, email TEXT, phone INTEGER, address TEXT, stated_at INTEGER)', // CREATE TABLE clients(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, surname TEXT, email TEXT, phone INTEGER, address TEXT, stated_at INTEGER),
        );
      },
      version: 1,
    );
    return db;
  }
}


