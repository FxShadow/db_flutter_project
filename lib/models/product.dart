import 'package:sqflite/sqflite.dart';

import '../helpers/db_helper.dart';

class Product {
  final int? id;
  final String name;
  final int price;
  final int quantity;
  final int stated_at;

  const Product({
    this.id,
    required this.name,
    required this.price,
    required this.quantity,        
    this.stated_at = 1
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'quantity': quantity,
      'stated_at': stated_at,
    };
  }
 
  @override
  String toString() {
    return 'Product{id: $id, name: $name, price: $price, quantity: $quantity, stated_at: $stated_at}';
  }

static Future<List<Product>> getProduct() async {
    final Database db = await DbHelper.initDb();
    final List<Map<String, dynamic>> maps = await db.query('products', where: "stated_at = 1");
    return List.generate(maps.length, (i) {
      return Product(
        id: maps[i]['id'],
        name: maps[i]['name'],
        price: maps[i]['price'],
        quantity: maps[i]['quantity'],
        stated_at: 1,        
      );
    });
  }

 static Future<int> insertProduct(Product Product) async {
    final Database db = await DbHelper.initDb();
    final int result = await db.insert('products', Product.toMap());
    return result;
  }

  static Future<int> updateProduct(Product Product) async {
    final Database db = await DbHelper.initDb();
    return await db.update(
      'products',
      Product.toMap(),
      where: 'id = ?',
      whereArgs: [Product.id],
    );
  }

  static Future<int> deleteProduct(int id) async {
    final Database db = await DbHelper.initDb();
    return await db.update(
      'products',
      {
        'stated_at': 0
      },
      where: 'id = ?',
      whereArgs: [id],
    );
  }

}