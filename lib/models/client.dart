import 'package:sqflite/sqflite.dart';

import '../helpers/db_helper.dart';

class Client {
  final int? id;
  final String name;
  final String surname;
  final String email;
  final int phone;
  final String address;
  final int stated_at;

  const Client({
    this.id,
    required this.name,
    required this.surname,
    required this.email,
    required this.phone,
    required this.address,
    this.stated_at = 1
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'surname': surname,
      'email': email,
      'phone': phone,
      'address': address,
      'stated_at': stated_at,
    };
  }
 
  @override
  String toString() {
    return 'Client{id: $id, name: $name, surname: $surname, email: $email, phone: $phone ,address: $address, stated_at: $stated_at}';
  }

static Future<List<Client>> getClient() async {
    final Database db = await DbHelper.initDb();
    final List<Map<String, dynamic>> maps = await db.query('Clients', where: "stated_at = 1");
    return List.generate(maps.length, (i) {
      return Client(
        id: maps[i]['id'],
        name: maps[i]['name'],
        surname: maps[i]['surname'],
        email: maps[i]['email'],
        phone: maps[i]['phone'],
        address: maps[i]['address'],
        stated_at: 1,        
      );
    });
  }

 static Future<int> insertClient(Client Client) async {
    final Database db = await DbHelper.initDb();
    final int result = await db.insert('Clients', Client.toMap());
    return result;
  }

  static Future<int> updateClient(Client Client) async {
    final Database db = await DbHelper.initDb();
    return await db.update(
      'Clients',
      Client.toMap(),
      where: 'id = ?',
      whereArgs: [Client.id],
    );
  }

  static Future<int> deleteClient(int id) async {
    final Database db = await DbHelper.initDb();
    return await db.update(
      'Clients',
      {
        'stated_at': 0
      },
      where: 'id = ?',
      whereArgs: [id],
    );
  }

}