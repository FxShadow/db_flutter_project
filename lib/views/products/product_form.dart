import 'package:db_flutter_activity/models/product.dart';
import 'package:db_flutter_activity/views/products/product_list.dart';
import 'package:flutter/material.dart';

class ProductForm extends StatefulWidget {
  const ProductForm({super.key});

  @override
  State<ProductForm> createState() => _ProductFormState();
}

class _ProductFormState extends State<ProductForm> {
  final _ProductFormKey = GlobalKey<FormState>();
  final _NameController = TextEditingController();
  final _PriceController = TextEditingController();
  final _QuantityController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("My Project :D")),
      body: Padding(
        padding: EdgeInsets.only(top: 50, left: 15, right: 15),
        child: Center(
          child: SingleChildScrollView(
            child: Form(
              key: _ProductFormKey,
              child: Column(
                children: [
                  const Center(
                    child: Text("Registrar Producto",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 25)),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    controller: _NameController,
                    decoration: InputDecoration(
                        labelText: "Nombre del Producto",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "No se puede enviar el nombre vacio";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: _PriceController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Precio del Producto",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "No se puede enviar el precio vacio";
                      } else if (int.parse(value) <= 50) {
                        return "El precio debe ser mayor a 50 pesos";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: _QuantityController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Cantidad",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "No se puede enviar la cantidad vacia";
                      } else if (int.parse(value) <= 0) {
                        return "La cantidad debe ser mayor a 0";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 25),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductList()));
                          },
                          child: const Text("Ir a la lista")),
                      const SizedBox(width: 25),
                      ElevatedButton(
                          onPressed: () {
                            if (_ProductFormKey.currentState!.validate()) {
                              submitProduct();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProductList()));
                            }
                          },
                          child: const Text("Crear Producto"))
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<ScaffoldFeatureController<SnackBar, SnackBarClosedReason>>
      submitProduct() async {
    final name = _NameController.text;
    final price = int.parse(_PriceController.text);
    final quantity = int.parse(_QuantityController.text);
    final product = Product(name: name, price: price, quantity: quantity);
    if (await Product.insertProduct(product) == 1) {
      return ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Color(0xFF81C784),
          content: Text('Producto Registrado Correctamente',
              textAlign: TextAlign.center)));
    } else {
      return ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Color.fromARGB(255, 233, 89, 89),
          content:
              Text('Hubo un Error al Registrar', textAlign: TextAlign.center)));
    }
  }
}
