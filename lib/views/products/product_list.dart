import 'package:db_flutter_activity/views/home_page.dart';
import 'package:db_flutter_activity/views/products/product_form.dart';
import 'package:flutter/material.dart';

import '../../models/product.dart';

class ProductList extends StatefulWidget {
  const ProductList({super.key});

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  List<Product> products = [];

  @override
  void initState() {
    loadProducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("My Project :D"),
        ),
        body: Padding(
            padding: EdgeInsets.only(top: 25, left: 15, right: 15),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(   
                      style: ElevatedButton.styleFrom(padding: EdgeInsets.all(10)),             
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductForm()));
                      },
                      child: const Text("Registrar Producto", style: TextStyle(fontSize: 20),)
                    ),
                    ElevatedButton(   
                      style: ElevatedButton.styleFrom(padding: EdgeInsets.all(10)),             
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePage()));
                      },
                      child: const Text("Inicio", style: TextStyle(fontSize: 20),)
                    )
                  ],
                ),
                SizedBox(height: 30),
                Expanded(
                    child: ListView.separated(
                  itemCount: products.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                  itemBuilder: (BuildContext context, int index) {
                    return Dismissible(
                      key: Key("${products[index].id}"),
                      background: Container(
                        color: Colors.red.shade400,
                        child: Row(children: [Icon(Icons.delete_forever, color: Colors.white), SizedBox(width: 10),Text("Eliminar Producto", style: TextStyle(color: Colors.white, fontSize: 18))],),
                      ),
                      child: ListTile(
                        title: Text(products[index].name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                        subtitle: Column(children: [
                          Text("Precio: \$ ${products[index].price}",style: TextStyle(fontSize: 16)),
                          Text("Cantidad: ${products[index].quantity}",style: TextStyle(fontSize: 16))
                        ]),
                      ),
                      onDismissed:(direction) async {
                        setState(() {
                          products.removeAt(index);
                        });
                        await Product.deleteProduct(products[index].id!);
                      },
                    );
                  },
                ))
              ],
            )));
  }

  void loadProducts() async {
    final productsList = await Product.getProduct();
    setState(() {
      products = productsList;
    });
  }
}
