import 'package:flutter/material.dart';

import 'clients/client_form.dart';
import 'products/product_form.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Project :D"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 50, left: 10, right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Container(
                  width: 75,
                  height: 75,
                  decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: IconButton(
                    iconSize: 45,
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ClientForm()));
                    }, 
                    icon: const Icon(Icons.person)
                  )
                ),
                SizedBox(height: 10),
                const Text("Clientes", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold))
              ],
            ),
            Column(
              children: [
                Container(
                  decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.all(Radius.circular(15))),
                  width: 75,
                  height: 75,
                  child: IconButton(
                    iconSize: 40,
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ProductForm()));
                    },
                   icon: const Icon(Icons.breakfast_dining)
                  ),
                ),
                SizedBox(height: 10),
                const Text("Productos", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold))
              ],
            )
          ],
        ),
      ),
    );
  }
}