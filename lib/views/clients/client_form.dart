import 'package:db_flutter_activity/models/client.dart';
import 'package:db_flutter_activity/views/clients/client_list.dart';
import 'package:flutter/material.dart';

class ClientForm extends StatefulWidget {
  const ClientForm({super.key});

  @override
  State<ClientForm> createState() => _ClientFormState();
}

class _ClientFormState extends State<ClientForm> {
  final _ClientFormKey = GlobalKey<FormState>();
  final _NameController = TextEditingController();
  final _SurnameController = TextEditingController();
  final _EmailController = TextEditingController();
  final _PhoneController = TextEditingController();
  final _AddressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("My Project :D")),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 50, left: 15, right: 15),
          child: Center(
            child: Form(
              key: _ClientFormKey,
              child: Column(
                children: [
                  const Center(
                    child: Text("Registrar Cliente",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 25)),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    controller: _NameController,
                    decoration: InputDecoration(
                        labelText: "Nombre",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "No se puede enviar el nombre vacio";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: _SurnameController,
                    decoration: InputDecoration(
                        labelText: "Apellidos",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "No se puede enviar los apellidos vacio";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: _EmailController,
                    decoration: InputDecoration(
                        labelText: "Correo Electronico",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "No se puede enviar el correo vacio";
                      } else if (!value.contains("@")) {
                        return "El correo debe ser valido";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: _PhoneController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Telefono",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "No se puede enviar el telefono vacio";
                      } else if (value.length == 10) {
                        return "El telefono debe tener 10 digitos";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: _AddressController,
                    decoration: InputDecoration(
                        labelText: "Direccion",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "No se puede enviar la direccion vacia";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 25),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ClientList()));
                          },
                          child: const Text("Ir a la lista")),
                      const SizedBox(width: 25),
                      ElevatedButton(
                          onPressed: () {
                            if (_ClientFormKey.currentState!.validate()) {
                              submitClient();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ClientList()));
                            }
                          },
                          child: const Text("Crear Cliente"))
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<ScaffoldFeatureController<SnackBar, SnackBarClosedReason>>
      submitClient() async {
    final name = _NameController.text;
    final surname = _SurnameController.text;
    final email = _EmailController.text;
    final phone = int.parse(_PhoneController.text);
    final address = _AddressController.text;

    final client = Client(
        name: name,
        surname: surname,
        email: email,
        phone: phone,
        address: address);

    if (await Client.insertClient(client) == 1) {
      return ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Color(0xFF81C784),
          content: Text('Cliente Registrado Correctamente',
              textAlign: TextAlign.center)));
    } else {
      return ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Color.fromARGB(255, 233, 89, 89),
          content:
              Text('Hubo un Error al Registrar', textAlign: TextAlign.center)));
    }
  }
}
