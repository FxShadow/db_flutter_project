import 'package:db_flutter_activity/views/clients/client_form.dart';
import 'package:flutter/material.dart';

import '../../models/client.dart';
import '../home_page.dart';

class ClientList extends StatefulWidget {
  const ClientList({super.key});

  @override
  State<ClientList> createState() => _ClientListState();
}

class _ClientListState extends State<ClientList> {
  List<Client> clients = [];

  @override
  void initState() {
    loadClients();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("My Project :D"),
        ),
        body: Padding(
            padding: EdgeInsets.only(top: 25, left: 15, right: 15),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(   
                      style: ElevatedButton.styleFrom(padding: EdgeInsets.all(10)),             
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ClientForm()));
                      },
                      child: const Text("Registrar Cliente", style: TextStyle(fontSize: 20),)
                    ),
                    ElevatedButton(   
                      style: ElevatedButton.styleFrom(padding: EdgeInsets.all(10)),             
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePage()));
                      },
                      child: const Text("Inicio", style: TextStyle(fontSize: 20),)
                    )
                  ],
                ),
                SizedBox(height: 30),
                Expanded(
                    child: ListView.separated(
                  itemCount: clients.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                  itemBuilder: (BuildContext context, int index) {                    
                    return Dismissible(
                      key: Key("${clients[index].id}"),
                      background: Container(
                        color: Colors.red.shade400,
                        child: Row(children: [Icon(Icons.delete_forever, color: Colors.white), SizedBox(width: 10),Text("Eliminar Cliente", style: TextStyle(color: Colors.white, fontSize: 18))],),
                      ),
                      child: ListTile(
                      title: Text("${clients[index].name} ${clients[index].surname}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                      subtitle: Column(children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text("correo: ${clients[index].email}",style: TextStyle(fontSize: 16)),
                            Text("telefono: ${clients[index].phone}",style: TextStyle(fontSize: 16))
                          ]
                        ),
                        Text("Direccion: ${clients[index].address}",style: TextStyle(fontSize: 16))
                      ]),
                    ),
                    onDismissed:(direction) async {
                        setState(() {
                          clients.removeAt(index);
                        });
                        await Client.deleteClient(clients[index].id!);
                      },
                    );
                  },
                ))
              ],
            )));
  }

    void loadClients() async {
    final clientsList = await Client.getClient();
    setState(() {
      clients = clientsList;
    });
  }
}